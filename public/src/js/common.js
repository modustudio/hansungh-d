$(function(){
    var gnb = $('#gnb');
    $(gnb).find('[class$=tit]').on('click keypress' ,function(e){
        if ((e.keyCode == 13)||(e.type == 'click')) {
            if (!$(gnb).hasClass('js-open-m')) {
                $(gnb).addClass('js-open-m');
            } else {
                $(gnb).removeClass('js-open-m');
            }
        }
    });
    $(gnb.selector+'>ul>li>a').on('click',function(e) {
		if ($(this).parents('li').hasClass('js-open-m')) {
			$(this).parents('li').removeClass('js-open-m');
		} else {
			$(this).parents('li').siblings().removeClass('js-open-m');
			$(this).parents('li').addClass('js-open-m');
		}
        if($(this).next().length != 0) e.preventDefault();
	});
    $(gnb.selector).find('a').last().on('blur',function(e) {
        $(gnb).removeClass('js-open-m');
    });
});

$(function(){
    $('#mainVisual').touchSlider({
        autoplay : {
			enable : true,
			//pauseHover : true,
			interval : 5000
		},
        initComplete : function (e) {
			var _this = this;
			var $this = $(this);
			var paging = $this.children('[class$=pagination]');

			paging.html('');
			$this.find(' > ul > li').each(function (i, el) {
				var num = (i+1) / _this._view;
				if((i+1) % _this._view == 0) {
					paging.append('<button type="button" class="js-bullet">visual' + num + '</button>');
				}
			});
			paging.find('.js-bullet').on('click', function (e) {
				_this.go_page($(this).index());
			});
		},
		counter : function (e) {
			$(this).children('[class$=pagination]').find('.js-bullet').removeClass('js-on').eq(e.current-1).addClass('js-on');
		}
    });
});
